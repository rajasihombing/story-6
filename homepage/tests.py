from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .views import index
from .models import Kegiatan
from .models import Peserta
from .models import *
from http import HTTPStatus
from .forms import KegiatanCreateForm, PesertaCreateForm
from .apps import HomepageConfig
from django.apps import apps

class Story6UnitTest(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_daftar_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "main csgo")
        Peserta.objects.create(nama_peserta = "damedane")
        hitung = Kegiatan.objects.all().count()
        hitung += Peserta.objects.all().count()
        self.assertEqual(hitung, 2)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "matdas")
        kegiatan  = Kegiatan.objects.get(nama_kegiatan = "matdas")
        self.assertEqual(str(kegiatan), "matdas")

    def test_peserta(self):
        Peserta.objects.create(nama_peserta = "mahfud")
        peserta = Peserta.objects.get(nama_peserta = "mahfud")
        self.assertEqual(str(peserta), "mahfud")

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_post_form(self):
            test_str = 'shalom'
            response_post = Client().post('', {'nama_kegiatan':"shalom"})
            self.assertEqual(response_post.status_code,200)
            kegiatan_form = KegiatanCreateForm(data={'nama_kegiatan':test_str})
            self.assertTrue(kegiatan_form.is_valid())
            self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"shalom")

    def test_valid_form(self):
        data = {'nama_kegiatan':"rebahan"}
        kegiatan_form = KegiatanCreateForm(data=data)
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"rebahan")

    